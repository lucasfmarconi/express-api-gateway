FROM node:11-alpine

COPY . .

RUN npm install

EXPOSE 8080

CMD [ "npm", "start" ]